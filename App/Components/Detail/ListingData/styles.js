/** @format */

import { StyleSheet, Platform, Dimensions } from 'react-native'
import { Color, Constants } from '@common'
const { width, height } = Dimensions.get('window')
const vw = width 
const vh = height 

export default StyleSheet.create({
  container: {
    backgroundColor: '#F0F0F0',
    alignItems: 'center',
    paddingBottom: 30,
  },

  // for section general
  section: {
    marginTop: 20,
    paddingVertical: 20,

    backgroundColor: '#FFF',
    width: "100%" ,
    shadowColor: '#000',
    shadowOpacity: 0.05,
    shadowRadius: 2,
    shadowOffset: { width: 0, height: 1 },
    borderRadius: 3,
  },
  sectionMap: {
    paddingBottom: 0
  },

  // content
  content: {
    width:"100%",
    paddingHorizontal: 22,
    paddingVertical: 10,
  },

  iconRow: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
    marginRight: 7,
    marginTop: 3,
    opacity: 0.5
  },

  // for section type is data
  row: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginVertical: 8,
    borderBottomWidth: 0.5,
    borderColor: '#F0F0F0',
    paddingVertical: 3,
  },
  label: {
    color: '#000',
    width: "100%",
    fontSize: 11,
    fontFamily: Constants.fontFamilyBold,
    lineHeight: 18,
    alignSelf: 'flex-start',
  },
  imageIcon: {
    tintColor: '#000',
    resizeMode: 'contain',
    width: 22,
    height: 22,
  },
  text: {
    color: '#000',
    fontSize: 12,
    lineHeight: 15,
    fontFamily: Constants.fontFamilyLight,
    alignSelf: 'flex-end',
  },

  boxItems: {
    marginHorizontal: 20,
    width:'90%',
  },
  //features
  boxFeature: {
    padding: 12,
    flexWrap: 'wrap',
    alignItems: 'stretch',
    flexDirection: 'row',
    marginBottom: 15,
  },
  lineTitle: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    paddingVertical: 10,
    paddingBottom: 20,
    paddingLeft: 20,
  },
  title: {
    width:'100%',
    fontSize: 15,
    color: 'rgb(69,69,83)',
    fontFamily: Constants.fontFamilyLight,
  },
  item: {
    flexDirection: 'row',
    marginVertical: 5,
    width: '100%',
    alignItems: 'center',
  },
  iconFeature: {
    marginRight: 5,
  },
  nameFeature: {
    fontSize: 13,
    width:'100%'
  },

  //cates
  lineTitleCates: {
    width: '40%',
  },

  //map
  lineTitleMap: {
    width: '60%',
  },
  lineMapRight: {
    width: '40%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textMap: {
    fontFamily: Constants.fontFamilyBold,
    fontSize: 12,
  },

  //related
  boxRelatedItems: {
   marginLeft: 0,
   marginRight:5
  }
})
