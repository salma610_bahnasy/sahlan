import React, { StyleSheet } from 'react-native'

export default StyleSheet.create({
  body: {
    alignItems: 'center',
    backgroundColor: '#435FAC',
    borderRadius: 40,
    paddingTop: 5,
    paddingRight: 5,
    paddingBottom: 5,
    paddingLeft: 5,
    marginBottom: 14,
  },
})
