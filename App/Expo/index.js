/** @format */

import * as Expo from 'expo'
import {
  Components
} from 'expo';
import { LinearGradient } from 'expo-linear-gradient';
import * as Font from 'expo-font';
import * as ImagePicker from 'expo-image-picker';
import * as Facebook from 'expo-facebook';
import * as Google from 'expo-google-app-auth';
import * as FacebookAds from 'expo-ads-facebook';
const AdSettings = FacebookAds.AdSettings;
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import * as StoreReview from 'expo-store-review';
import * as WebBrowser from 'expo-web-browser';
import MapView from 'react-native-maps';
import * as Location from 'expo-location';

export {
  Font,
  Components,
  WebBrowser,
  LinearGradient,
  FacebookAds,
  AdSettings,
  Google,
  Facebook,
  ImagePicker,
  Permissions,
  StoreReview,
  Constants,
  MapView,
  Location
}
export default Expo
