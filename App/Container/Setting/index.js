/** @format */

'use strict'

import React, { Component } from 'react'
import { Text, ScrollView, View, } from 'react-native'
import { Slider } from '@components'
import css from './style'
import { Languages } from '@common'
import LangSwitcher from './LangSwitch'

export default class Setting extends Component {

  render() {
    return (
      <ScrollView >
        <View style={{ width: '90%', alignSelf: 'center' }}>
          <Text style={{ fontSize: 18, marginVertical: 20, color: 'red' }}>
            من نحن ؟
          </Text>
        
            <Text style={{ lineHeight: 30 }} >
              نحن شركة System Online ، شركة استشارات تكنولوجيا المعلومات وتطوير البرمجيات معروفة بخبرتنا الصناعية العميقة ورضا العملاء المرتفع. نحن نتعاون مع العملاء لحل تحدياتهم الأكثر إلحاحًا من الاستراتيجية حتى التنفيذ.
              نحن نقدم الاستشارات الاستراتيجية ، وتطوير التطبيقات ، والحلول التقنية ، والاختبار المستقل ، وهندسة المنتجات والخدمات المدارة لمساعدة عملائنا على الفوز في السوق العالمية.
              تم تصميم عروض خدماتنا لتوفير التمايز الاستراتيجي والتفوق التشغيلي لعملائنا. نحن نقدم حلول تكنولوجيا المعلومات الصحيحة التي تبني القيمة ، وتتغلب على المنافسة وتولد تأثيرًا ماليًا كبيرًا ودائمًا لعملائنا.
              نحن نفخر ببناء علاقات إستراتيجية طويلة الأمد مع العملاء. أكثر من 90٪ من عائداتنا تأتي من العملاء الحاليين.
          </Text>
            <Text style={{ fontSize:18, marginVertical: 10, color: 'red' }}>
              اتصل بنا
          </Text>
            <View style={{  marginTop: 10 }}>
              <Text style={{ fontSize: 14, fontWeight: 'bold', marginHorizontal: 5 }}>التليفون : </Text>
              <Text> 01002799299</Text>

            </View>
            <View >
              <Text style={{ fontSize: 14, fontWeight: 'bold', marginHorizontal: 5 }}>البريد الإلكترونى : </Text>
              <Text> info@system-online.com </Text>

            </View>

          </View>
       
      </ScrollView>
    )
  }
}
