/** @format */

import React, { Component } from 'react'
import { View, WebView, Text, ActivityIndicator } from 'react-native'
import { Menu } from './Icons'
import { Constants } from '@common'
import { CustomPage } from '@container'
import { Toolbar } from '@components'
import { SafeAreaView } from 'react-navigation';
import * as Permissions from 'expo-permissions';

export default class CustomPageScreen extends Component {
  state = {
    visible: true
  }
  static navigationOptions = ({ navigation }) => ({
    title:
      typeof navigation.state.params != 'undefined'
        ? navigation.state.params.title
        : '',
    headerLeft: Constants.RTL ? null : Menu(),
    headerRight: Constants.RTL ? Menu() : null,
  })
  componentDidMount = () => {

    const { status } = Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status !== 'granted') {
      alert('Sorry, we need camera roll permissions to make this work!');
    }
  }
  showSpinner() {
    console.log('Show Spinner');
    this.setState({ visible: true });
  }

  hideSpinner() {
    console.log('Hide Spinner');
    this.setState({ visible: false });
  }

  render() {
    const { state } = this.props.navigation
    const { url, id } = state.params
    console.log(state.params)
    if (typeof state.params === 'undefined') {
      return <View ><Text>undefined CustomPage</Text></View>
    }

    if (typeof url !== 'undefined' && url.length > 0) {
      return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFF' }}>
          <Toolbar />
          <WebView source={{ uri: url }}

            //Enable Javascript support
            javaScriptEnabled={true}
            //For the Cache
            domStorageEnabled={true}
            //View to show while loading the webpage
            //Want to show the view or not
            //startInLoadingState={true}
            onLoadStart={() => this.showSpinner()}
            onLoad={() => this.hideSpinner()}
          />
          {this.state.visible ? (
            <ActivityIndicator
              color="#009688"
              size="large"

            />
          ) : null}
        </SafeAreaView>
      )
    }
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomPage id={id} />
      </SafeAreaView>
    )
  }
}
